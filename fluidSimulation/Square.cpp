#include "Square.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

Square::Square()
{
}

Square::~Square()
{
}

void Square::init(GLSLShader* shader)
{
	//initialise all variables
	m_pNumVertices = 4;
	m_pNumIndices = 6;
	m_pPolygonMode = GL_LINE;
	m_pRendererMode = GL_TRIANGLES;

	//setup square vertices and geometry
	m_pVertices[0].color.r = 0;
	m_pVertices[0].color.g = 0;
	m_pVertices[0].color.b = 0;
	m_pVertices[0].color.a = 255;

	m_pVertices[1].color.r = 0;
	m_pVertices[1].color.g = 0;
	m_pVertices[1].color.b = 0;
	m_pVertices[1].color.a = 255;

	m_pVertices[2].color.r = 0;
	m_pVertices[2].color.g = 0;
	m_pVertices[2].color.b = 0;
	m_pVertices[2].color.a = 255;

	m_pVertices[3].color.r = 0;
	m_pVertices[3].color.g = 0;
	m_pVertices[3].color.b = 0;
	m_pVertices[3].color.a = 255;

	m_pVertices[0].position.x = -0.5;
	m_pVertices[0].position.y = -0.5;
	m_pVertices[0].position.z = 0;

	m_pVertices[1].position.x = +0.5;
	m_pVertices[1].position.y = -0.5;
	m_pVertices[1].position.z = 0;

	m_pVertices[2].position.x = +0.5;
	m_pVertices[2].position.y = +0.5;
	m_pVertices[2].position.z = 0;

	m_pVertices[3].position.x = -0.5;
	m_pVertices[3].position.y = +0.5;
	m_pVertices[3].position.z = 0;

	//setup square indices
	m_pIndices[0] = 0;
	m_pIndices[1] = 1;
	m_pIndices[2] = 2;
	m_pIndices[3] = 0;
	m_pIndices[4] = 2;
	m_pIndices[5] = 3;

	m_pPosition = glm::vec3(0);
	m_pRotation = glm::vec3(0);
	m_pScale = glm::vec3(10);

	m_pTranslationVector = m_pPosition;
	m_pRotationVector = m_pRotation;
	m_pScaleVector = m_pScale;

	m_pTranslationMatrix = glm::translate(glm::mat4(), m_pTranslationVector);
	m_pRotationMatrix = glm::eulerAngleYXZ(m_pRotationVector.y, m_pRotationVector.x, m_pRotationVector.z);
	m_pScalingMatrix = glm::scale(glm::mat4(), m_pScaleVector);

	m_pModelMatrix = m_pTranslationMatrix * m_pRotationMatrix * m_pScalingMatrix;

	//call mesh init
	m_pSquareMesh.init(m_pVertices, m_pNumVertices, m_pIndices, m_pNumIndices, shader);
}

void Square::update()
{
	
}

void Square::render(GLSLShader* shader)
{
	m_pSquareMesh.render(m_pPolygonMode, m_pRendererMode, m_pNumIndices, shader, 1, &m_pModelMatrix);
}

void Square::close()
{
	m_pSquareMesh.close();
}
