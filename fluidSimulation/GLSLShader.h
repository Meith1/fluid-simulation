#ifndef __GLSLShader__
#define __GLSLShader__

#include <GL/glew.h>
#include <string>
#include <map>

using namespace std;

class GLSLShader
{
public:
	GLSLShader();
	~GLSLShader();
	void loadFromString(GLenum shaderType, const string& source);
	void loadFromFile(GLenum shderType, const string& filename);
	void createAndLinkProgram();
	void use();
	void unUse();
	void addAttribute(const string& attribute);
	void addUniform(const string& uniform);

	GLuint operator[](const string& attribute);
	GLuint operator()(const string& uniform);
	void deleteShaderProram();

private:
	enum m_pShaderType {VERTEX_SHADER, FRAGMENT_SHADER, GEOMETRY_SHADER};
	GLuint m_pProgram;
	int m_pTotalShaders;
	GLuint m_pShaders[3];
	map<string, GLuint> m_pAttributeList;
	map<string, GLuint> m_pUniformLocationList;
};

#endif
