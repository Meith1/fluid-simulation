#ifndef __Suare__
#define __Square__

#include "Mesh.h"
#include "GLSLShader.h"

class Square
{
public:
	Square();
	~Square();

	void init(GLSLShader* shader);
	void update();
	void render(GLSLShader* shader);
	void close();

private:

	Mesh m_pSquareMesh;
	Vertex m_pVertices[4];
	int m_pNumVertices;
	GLushort m_pIndices[6];
	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;

	glm::vec3 m_pPosition;
	glm::vec3 m_pRotation;
	glm::vec3 m_pScale;
	glm::vec3 m_pVelocity;

	glm::vec3 m_pTranslationVector;
	glm::vec3 m_pRotationVector;
	glm::vec3 m_pScaleVector;

	glm::mat4 m_pRotationMatrix;
	glm::mat4 m_pTranslationMatrix;
	glm::mat4 m_pScalingMatrix;
	glm::mat4 m_pModelMatrix;
};

#endif
