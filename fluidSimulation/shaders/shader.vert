#version 330 core
 
layout(location = 0) in vec3 vVertex;	//object space vertex position
layout(location = 1) in vec3 vColor;	//per-vertex colour
layout(location = 2) in mat4 vModelMatrix; //model matrix of each object

//output from the vertex shader
smooth out vec4 vSmoothColor;		//smooth colour to fragment shader

//uniform
uniform mat4 V;
uniform mat4 P;

void main()
{
	//assign the per-vertex colour to vSmoothColor varying
   vSmoothColor = vec4(vColor,1);

   //get the clip space position by multiplying the combined MVP matrix with the object space 
   //vertex position
   gl_Position = P * V * vModelMatrix * vec4(vVertex,1);
}