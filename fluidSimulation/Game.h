#ifndef __Game__
#define __Game__

#include "GLSLShader.h"
#include "Circle.h"
#include "Square.h"

#include <SDL/SDL.h>

class Game
{
public:
	
	static Game* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new Game();
			return s_pInstance;
		}

		return s_pInstance;
	}

	bool init();
	void handleEvents();
	void update();
	void render();
	void close();
	void quit();

	bool running() { return  m_bRunning; }

	void setCircleVelFromGrid();

private:
	Game();
	~Game();

	static Game* s_pInstance;
	bool m_bRunning;

	GLSLShader* m_pShader;

	Circle m_pCircleGameObject;
	Square m_pSquareGameObject;

	glm::vec2* positionsOfCircles;
};

#endif
