#include "Mesh.h"
#include "Camera.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <iostream>

#define MAX_CIRCLES 2

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

void Mesh::init(Vertex vertices[], int numVertices, GLushort indices[], int numIndices, GLSLShader* shader)
{
	//setup mesh vao and vbo stuff
	glGenVertexArrays(1, &m_pVaoID);
	glGenBuffers(1, &m_pVboID);
	glGenBuffers(1, &m_pVioID);
	GLsizei stride = sizeof(Vertex);

	//bind vao and vbo stuff
	glBindVertexArray(m_pVaoID);

	glBindBuffer(GL_ARRAY_BUFFER, m_pVboID);

	//pass mesh vertices to buffer objects
	glBufferData(GL_ARRAY_BUFFER, stride*numVertices, &vertices[0], GL_STATIC_DRAW);

	//enable vertex attribute array for position
	glEnableVertexAttribArray((*shader)["vVertex"]);
	glVertexAttribPointer((*shader)["vVertex"], 3, GL_FLOAT, GL_FALSE, stride, 0);

	//enable vertex attribute for color
	glEnableVertexAttribArray((*shader)["vColor"]);
	glVertexAttribPointer((*shader)["vColor"], 3, GL_FLOAT, GL_FALSE, stride, (const GLvoid*)offsetof(Vertex, color));

	//pass indices to element array buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pVioID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(GLushort), &indices[0], GL_STATIC_DRAW);

	// create world matrix buffer
	glGenBuffers(1, &m_pModelMatrixBufferID);
	
	glBindBuffer(GL_ARRAY_BUFFER, m_pModelMatrixBufferID);

	for (int i = 0; i < 4; i++)
	{
		glEnableVertexAttribArray((*shader)["vModelMatrix"] + i);
		glVertexAttribPointer((*shader)["vModelMatrix"] + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (const GLvoid*) (i * sizeof(glm::vec4)));
		glVertexAttribDivisor((*shader)["vModelMatrix"] + i, 1);
	}

	std::cout << "Mesh init successful.\n";
}

void Mesh::update()
{
	
}

void Mesh::render(GLenum polygonMode, GLenum renderMode, int numIndices, GLSLShader* shader, int numInstances, glm::mat4* m_pModelMatrix)
{
	shader->use();
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_pModelMatrixBufferID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * numInstances, &m_pModelMatrix[0][0], GL_STATIC_DRAW);
		glBindVertexArray(m_pVaoID);
		glUniformMatrix4fv((*shader)("V"), 1, GL_FALSE, &Camera::Instance()->getViewMatrix()[0][0]);
		glUniformMatrix4fv((*shader)("P"), 1, GL_FALSE, &Camera::Instance()->getProjectionMatrix()[0][0]);
		{
			//set polygon mode
			glPolygonMode(GL_FRONT_AND_BACK, polygonMode);
			//draw mesh
			glDrawElementsInstanced(renderMode, numIndices, GL_UNSIGNED_SHORT, 0, numInstances);
		}
	}
	shader->unUse();
}

void Mesh::close()
{
	//Destroy vao and vbo
	glDeleteBuffers(1, &m_pVboID);
	glDeleteBuffers(1, &m_pVioID);
	glDeleteVertexArrays(1, &m_pVaoID);

	cout << "mesh close successful\n";
}