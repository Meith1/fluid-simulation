#ifndef __GridCell__
#define __GridCell__

#include <random>

#include <glm/glm.hpp>

#define IX(i,j) ((i)+(N+2)*(j))
#define SWAP(x0,x) {float * tmp=x0;x0=x;x=tmp;}

const int N = 98; 

class GridCell
{

public:

	static GridCell* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new GridCell();
			return s_pInstance;
		}

		return s_pInstance;
	}

	void clearData();

	void dens_step(int N, float * x, float * x0, float * u, float * v, float diff, float dt);
	void vel_step(int N, float * u, float * v, float * u0, float * v0, float visc, float dt);

	void add_source(int N, float * x, float * s, float dt);
	void set_bnd(int N, int b, float * x);
	void lin_solve(int N, int b, float * x, float * x0, float a, float c);
	void diffuse(int N, int b, float * x, float * x0, float diff, float dt);
	void advect(int N, int b, float * d, float * d0, float * u, float * v, float dt);
	void project(int N, float * u, float * v, float * p, float * div);
	void get_from_UI();
	void update();

	glm::vec2 gridGetVelocity(int index) { return  glm::vec2(u[index], v[index]); }

private:
	static GridCell* s_pInstance;

	GridCell();
	~GridCell();

	// navier stokes stuff single values
	float dt, diff, visc;
	float force, source;

	int size;

	// navier stokes stuff per grid values
	float* u, *v, *u_prev, *v_prev;
	float* dens, *dens_prev;
};

#endif
