#ifndef __Mesh__
#define __Mesh__

#include "Vertex.h"
#include "GLSLShader.h"
#include "Camera.h"

#include <GL/glew.h>
#include <glm/glm.hpp>

class Mesh
{
public:
	Mesh();
	~Mesh();

	void init(Vertex vertices[], int numVertices, GLushort indices[], int numIndices, GLSLShader* shader);
	void update();
	void render(GLenum polygonMode, GLenum renderModem, int numIndices, GLSLShader* shader, int numInstances, glm::mat4* m_pModelMatrix);
	void close();

private:
	GLuint m_pVaoID;
	GLuint m_pVboID;
	GLuint m_pVioID;
	GLuint m_pModelMatrixBufferID;
};

#endif