#ifndef __Circle__
#define __Circle__

#include "Mesh.h"
#include "GLSLShader.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

const int SLICE = 5;
const float STEP = 2 * glm::pi <float>() / SLICE;

const int CIRCLES = 5000;

class Circle
{
public:
	Circle();
	~Circle();

	void init(GLSLShader* shader);
	void update();
	void render(GLSLShader* shader);
	void close();

	void incrementDisplay() { if(display < CIRCLES)display++; }

	glm::vec2* getPositions() { return m_pPosition; }
	void setVelByIndex(int index, glm::vec2 vel);

private:

	Mesh m_pCircleMesh;
	Vertex m_pVertices[SLICE+1];
	int m_pNumVertices;
	GLushort m_pIndices[3 * SLICE];
	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;

	float m_pRadius;
	float m_pMass;
	glm::vec2 m_pPosition[CIRCLES];
	glm::vec2 m_pRotation;
	glm::vec2 m_pScale;
	glm::vec2 m_pVelocity[CIRCLES];

	glm::vec2 m_pTranslationVector[CIRCLES];
	glm::vec2 m_pRotationVector;
	glm::vec2 m_pScaleVector;

	glm::mat4 m_pRotationMatrix;
	glm::mat4 m_pTranslationMatrix[CIRCLES];
	glm::mat4 m_pScalingMatrix;
	glm::mat4 m_pModelMatrix[CIRCLES];

	int display;
};

#endif
