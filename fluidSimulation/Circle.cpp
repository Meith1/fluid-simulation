#include "Circle.h"

#include <cmath>
#include <random>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

Circle::Circle()
{
}

Circle::~Circle()
{
}

void Circle::init(GLSLShader* shader)
{
	//initialise all variables
	m_pNumVertices = SLICE + 1;
	m_pNumIndices = 3 * SLICE;
	m_pPolygonMode = GL_FILL;
	m_pRendererMode = GL_TRIANGLE_FAN;

	display = CIRCLES;

	float angle = 0;
	m_pRadius = 0.5f;
	m_pVertices[0].position.x = m_pVertices[0].position.y = m_pVertices[0].position.z = 0;
	for (int i = 1; i <= SLICE; i++, angle = angle + STEP)
	{
		m_pVertices[i].position.x = m_pRadius*cos(angle);
		m_pVertices[i].position.y = m_pRadius*sin(angle);
		m_pVertices[i].position.z = 0;
	}

	for (int i = 0; i <= SLICE; i++)
	{
		m_pVertices[i].color.r = 0;
		m_pVertices[i].color.g = 0;
		m_pVertices[i].color.b = 255;
		m_pVertices[i].color.a = 255;
	}
	m_pVertices[0].color.r = 0;
	m_pVertices[0].color.g = 0;
	m_pVertices[0].color.b = 255;

	int prev = 1;
	
	for (int i = 0; i < 3 * SLICE; i+= 3)
	{
		m_pIndices[i] = 0;
		m_pIndices[i + 1] = prev;
		m_pIndices[i + 2] = prev + 1;
		prev++;
	}

	m_pIndices[(3 * SLICE) - 1] = 1;

	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution(-0.49f, +0.49f);

	for (int i = 0; i < CIRCLES; i++)
	{
		m_pPosition[i] = glm::vec2(distribution(generator) * 10, distribution(generator) * 10);
		m_pTranslationVector[i] = m_pPosition[i];
		m_pVelocity[i] = glm::vec2(0.f);
	}

	m_pRotation = glm::vec2(0);
	m_pScale = glm::vec2(0.1f);
	m_pMass = (m_pRadius*m_pRadius) * (m_pScale.x * m_pScale.x);

	m_pRotationVector = m_pRotation;
	m_pScaleVector = m_pScale;

	m_pRotationMatrix = glm::eulerAngleYXZ(m_pRotationVector.y, m_pRotationVector.x, 0.f);
	m_pScalingMatrix = glm::scale(glm::mat4(), glm::vec3(m_pScaleVector, 0.f));

	//call mesh init
	m_pCircleMesh.init(m_pVertices, m_pNumVertices, m_pIndices, m_pNumIndices, shader);
}

void Circle::setVelByIndex(int index, glm::vec2 vel)
{
	m_pVelocity[index] = vel;
}

void Circle::update()
{
	for (int i = 0; i < display; i++)
	{
		m_pPosition[i] += m_pVelocity[i];
		m_pTranslationVector[i] = m_pPosition[i];
		m_pTranslationMatrix[i] = glm::translate(glm::mat4(), glm::vec3(m_pTranslationVector[i],0));
		m_pModelMatrix[i] = m_pTranslationMatrix[i] * m_pRotationMatrix * m_pScalingMatrix;
	}	
}

void Circle::render(GLSLShader* shader)
{
	m_pCircleMesh.render(m_pPolygonMode, m_pRendererMode, m_pNumIndices, shader, display, m_pModelMatrix);
}

void Circle::close()
{
	m_pCircleMesh.close();
}
