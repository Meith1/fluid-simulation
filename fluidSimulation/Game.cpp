#include "Game.h"
#include "Window.h"
#include "InputHandler.h"
#include "Camera.h"
#include "GridCell.h"

#include <SDL/SDL.h>

#include <iostream>
#include <random>

Game* Game::s_pInstance = 0;

Game::Game()
{
	m_pShader = new GLSLShader();
	positionsOfCircles = new glm::vec2[CIRCLES];
}

Game::~Game()
{
}

bool Game::init()
{
	Window::Instance()->createWindow("Physics Engine", 1024, 768, 0);

	//load shader
	m_pShader->loadFromFile(GL_VERTEX_SHADER, "shaders/shader.vert");
	m_pShader->loadFromFile(GL_FRAGMENT_SHADER, "shaders/shader.frag");

	//compile and link shader
	m_pShader->createAndLinkProgram();
	m_pShader->use();
	{
		//add attributes and uniforms
		m_pShader->addAttribute("vVertex");
		m_pShader->addAttribute("vColor");
		m_pShader->addAttribute("vModelMatrix");
		m_pShader->addUniform("V");
		m_pShader->addUniform("P");
	}
	m_pShader->unUse();

	m_pCircleGameObject.init(m_pShader);
	m_pSquareGameObject.init(m_pShader);

	GridCell::Instance()->clearData();

	m_bRunning = true;
	return true;
}

void Game::handleEvents()
{
	glm::vec3 veclocity = glm::vec3(1.0f);

	InputHandler::Instance()->update();

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_W))
	{
		glm::vec3 displacement = Camera::Instance()->CamLookDirection() * veclocity;
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() + displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_S))
	{
		glm::vec3 displacement = Camera::Instance()->CamLookDirection() * veclocity;
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() - displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_D))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() + displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_A))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() - displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_F))
	{
		GridCell::Instance()->get_from_UI();
	}
}

void Game::setCircleVelFromGrid()
{
	positionsOfCircles = m_pCircleGameObject.getPositions();
	for (int i = 0; i < CIRCLES; ++i)
	{
		glm::vec2 posInGrid;
		posInGrid.x = abs(positionsOfCircles[i].x - (-5.f));
		posInGrid.y = abs(positionsOfCircles[i].y - (+5.f));
		posInGrid.x *= (N + 2) / 10;
		posInGrid.y *= (N + 2) / 10;
 		//std::cout << posInGrid.x << " " << posInGrid.y << "\n";
		//std::cout << (N + 2) * (int)posInGrid.y + (int)posInGrid.x << "\n";
		m_pCircleGameObject.setVelByIndex(i, GridCell::Instance()->gridGetVelocity((N+2) * (int)posInGrid.y + (int)posInGrid.x));
		//(i)+(N + 2)*(j)
	}
}

void Game::update()
{
	setCircleVelFromGrid();
	m_pCircleGameObject.update();
	m_pSquareGameObject.update();
	GridCell::Instance()->update();
	Camera::Instance()->update();
	//GridCell::Instance()->clearData();
}

void Game::render()
{
	glClearDepth(1.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_pCircleGameObject.render(m_pShader);
	m_pSquareGameObject.render(m_pShader);

	Window::Instance()->swapBuffers();
}

void Game::close()
{
	m_pCircleGameObject.close();

	//Destroy shader
	m_pShader->deleteShaderProram();

	SDL_DestroyWindow(Window::Instance()->getwindow());
}

void Game::quit()
{
	m_bRunning = false;
	SDL_Quit();
}
