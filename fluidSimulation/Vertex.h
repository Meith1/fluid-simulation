#ifndef __Vertex__
#define __Vertex__

#include <glm/glm.hpp>

struct Position
{
	float x, y, z;
};

struct Color
{
	float r, g, b, a;
};

struct Vertex
{
	Position position;
	Color color;
};

#endif